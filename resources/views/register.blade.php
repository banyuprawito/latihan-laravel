<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
        @csrf
        <label >First Name:</label> <br>
        <input type="text" name="namaDepan"> <br> <br>
        <label >Last Name:</label> <br>
        <input type="text" name="namaBelakang"> <br> <br>
        <label for="">Gender</label><br><br>
        <input type="radio" name="gender">Man <br>
        <input type="radio" name="gender">Woman <br>
        <input type="radio" name="gender">Other <br><br>
        <label>Nationality</label>
        <select name="nationality"><br><br>
            <option value="algeria">Indonesia</option>
            <option value="indonesia">Malaysia</option>
            <option value="zimbabwe">Zimbabwe</option>
        </select> <br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa">English <br>
        <input type="checkbox" name="bahasa">Arabic <br>
        <input type="checkbox" name="bahasa">Japanse <br><br>
        <label >Bio</label><br><br>
        <textarea name="message" cols="30" rows="10"></textarea>
        <br><br>
        <input type="submit" value="Sign Up">

    </form>
</body>
</html>